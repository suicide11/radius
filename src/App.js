
import React, { useEffect, useState } from 'react';
import './App.css';


function App() {

  const [total, setTotal] = useState(['grey', 'grey', 'grey', 'grey', 'grey', 'grey', 'grey'])
  const [stack, handleStack] = useState([])
  const [flag, setFlag] = useState(false)
  const handleClick = (id) => {

    let tempTotal = [...total]
    let stackTemp = [...stack, id]
    if (tempTotal[id] != 'green') {
      tempTotal[id] = 'green'
      stackTemp = [...stack, id]
      handleStack(stackTemp)
      setTotal(tempTotal)
    }


  }


  useEffect(() => {

      if (stack.length == total.length) {

        setFlag(true)
        setTimeout(() => {
          let temp = [...stack]
          let total1 = [...total]
          total1[temp.pop()] = 'grey'

          handleStack(temp)
          setTotal(total1)
        }, 500);
      }
      if (stack.length == 0) {
        setFlag(false)
      }
      if (flag == true) {

        setTimeout(() => {
          let temp = [...stack]
          let total1 = [...total]
          total1[temp.pop()] = 'grey'

          handleStack(temp)
          setTotal(total1)
        }, 500);
      }

  }, [stack])

  return (
    <div>
      
{/* asdasdasd */}
      <div className='box-div'>
        {total.map((value, index) => {
         return index < 3 ?  <div className='ColorBox' onClick={() => handleClick(index)} key={index} style={{ background: value }} >

          </div> : <React.Fragment></React.Fragment>
        })}
      </div>

      <div className='box-div'>
        {total.map((value, index) => {
         return index == 3 ?  <div className='ColorBox' onClick={() => handleClick(index)} key={index} style={{ background: value }} >

          </div> : <React.Fragment></React.Fragment>
        })}
      </div>


      <div className='box-div'>
        {total.map((value, index) => {
         return (index >3 && index < 7 ) ?  <div className='ColorBox' onClick={() => handleClick(index)} key={index} style={{ background: value }} >

          </div> : <React.Fragment></React.Fragment>
        })}
      </div>
    </div>
  );
}

export default App;
